set(JMUTILHEADERS
  ${JMUTIL_INCL_DIR}/JM/jm_callbacks.h
  ${JMUTIL_INCL_DIR}/JM/jm_vector.h
  ${JMUTIL_INCL_DIR}/JM/jm_vector_template.h
  ${JMUTIL_INCL_DIR}/JM/jm_stack.h
  ${JMUTIL_INCL_DIR}/JM/jm_types.h
  ${JMUTIL_INCL_DIR}/JM/jm_named_ptr.h
  ${JMUTIL_INCL_DIR}/JM/jm_string_set.h
  ${JMUTIL_INCL_DIR}/JM/jm_portability.h
  ${JMUTIL_INCL_DIR}/FMI/fmi_version.h
  ${JMUTIL_INCL_DIR}/FMI/fmi_util.h

  ${JMUTIL_INCL_DIR}/FMI1/fmi1_functions.h
  ${JMUTIL_INCL_DIR}/FMI1/fmi1_types.h
  ${JMUTIL_INCL_DIR}/FMI1/fmi1_enums.h

  ${JMUTIL_INCL_DIR}/FMI2/fmi2_functions.h
  ${JMUTIL_INCL_DIR}/FMI2/fmi2_types.h
  ${JMUTIL_INCL_DIR}/FMI2/fmi2_enums.h
  ${JMUTIL_INCL_DIR}/FMI2/fmi2_xml_callbacks.h
)
