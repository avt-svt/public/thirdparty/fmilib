set(JMUTILSOURCE
 ${JMUTIL_SOURCE_DIR}/JM/jm_callbacks.c
 ${JMUTIL_SOURCE_DIR}/JM/jm_templates_inst.c
 ${JMUTIL_SOURCE_DIR}/JM/jm_named_ptr.c
 ${JMUTIL_SOURCE_DIR}/JM/jm_portability.c
 
 ${JMUTIL_SOURCE_DIR}/FMI/fmi_version.c
 ${JMUTIL_SOURCE_DIR}/FMI/fmi_util.c
 
 ${JMUTIL_SOURCE_DIR}/FMI1/fmi1_enums.c
 ${JMUTIL_SOURCE_DIR}/FMI2/fmi2_enums.c
)
