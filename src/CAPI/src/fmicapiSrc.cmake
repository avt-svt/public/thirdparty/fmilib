set(FMICAPISOURCE
    ${FMICAPI_SOURCE_DIR}/FMI1/fmi1_capi_cs.c
    ${FMICAPI_SOURCE_DIR}/FMI1/fmi1_capi_me.c
    ${FMICAPI_SOURCE_DIR}/FMI1/fmi1_capi.c
    ${FMICAPI_SOURCE_DIR}/FMI2/fmi2_capi_cs.c
    ${FMICAPI_SOURCE_DIR}/FMI2/fmi2_capi_me.c
    ${FMICAPI_SOURCE_DIR}/FMI2/fmi2_capi.c
)
set(FMICAPISRCHEADERS
    ${FMICAPI_SOURCE_DIR}/FMI1/fmi1_capi_impl.h
    ${FMICAPI_SOURCE_DIR}/FMI2/fmi2_capi_impl.h
)
