set(FMIIMPORTSOURCE
	${FMIIMPORT_SRC_DIR}/FMI/fmi_import_context.c
	${FMIIMPORT_SRC_DIR}/FMI/fmi_import_util.c
	
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_cosim.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_capi.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_type.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_unit.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_variable.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_variable_list.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_vendor_annotations.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_capabilities.c
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_convenience.c

	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_capi.c
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_type.c
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_unit.c
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_variable.c
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_variable_list.c
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import.c
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_convenience.c
	)
	
set(FMIIMPORT_PRIVHEADERS
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_impl.h
	${FMIIMPORT_SRC_DIR}/FMI1/fmi1_import_variable_list_impl.h

	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_impl.h
	${FMIIMPORT_SRC_DIR}/FMI2/fmi2_import_variable_list_impl.h
 )
