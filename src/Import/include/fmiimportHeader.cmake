set(FMIIMPORT_PUBHEADERS
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_capi.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_capabilities.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_cosim.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_type.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_unit.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_variable.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_variable_list.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_vendor_annotations.h
	${FMIIMPORT_INCL_DIR}/FMI1/fmi1_import_convenience.h

	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import.h
	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import_capi.h
	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import_type.h
	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import_unit.h
	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import_variable.h
	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import_variable_list.h
	${FMIIMPORT_INCL_DIR}/FMI2/fmi2_import_convenience.h

	${FMIIMPORT_INCL_DIR}/FMI/fmi_import_context.h
	${FMIIMPORT_INCL_DIR}/FMI/fmi_import_util.h
 )
							
