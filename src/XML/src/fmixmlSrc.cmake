set(FMIXMLSRCHEADERS
	src/FMI/fmi_xml_context_impl.h

    src/FMI1/fmi1_xml_model_description_impl.h
    src/FMI1/fmi1_xml_parser.h
    src/FMI1/fmi1_xml_type_impl.h
    src/FMI1/fmi1_xml_unit_impl.h
    src/FMI1/fmi1_xml_vendor_annotations_impl.h
    src/FMI1/fmi1_xml_variable_impl.h
    src/FMI1/fmi1_xml_capabilities_impl.h

    src/FMI2/fmi2_xml_model_description_impl.h
    src/FMI2/fmi2_xml_model_structure_impl.h
    src/FMI2/fmi2_xml_parser.h
    src/FMI2/fmi2_xml_type_impl.h
    src/FMI2/fmi2_xml_unit_impl.h
    src/FMI2/fmi2_xml_variable_impl.h
 )

set(FMIXMLSOURCE
	src/FMI/fmi_xml_context.c
	
    src/FMI1/fmi1_xml_parser.c
    src/FMI1/fmi1_xml_model_description.c
    src/FMI1/fmi1_xml_type.c
    src/FMI1/fmi1_xml_unit.c
    src/FMI1/fmi1_xml_vendor_annotations.c
    src/FMI1/fmi1_xml_variable.c
    src/FMI1/fmi1_xml_capabilities.c
    src/FMI1/fmi1_xml_cosim.c

    src/FMI2/fmi2_xml_parser.c
    src/FMI2/fmi2_xml_model_description.c
    src/FMI2/fmi2_xml_model_structure.c
    src/FMI2/fmi2_xml_type.c
    src/FMI2/fmi2_xml_unit.c
	src/FMI2/fmi2_xml_vendor_annotations.c
	src/FMI2/fmi2_xml_variable.c
)
