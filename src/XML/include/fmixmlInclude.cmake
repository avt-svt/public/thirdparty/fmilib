set(FMIXMLINCLHEADERS
	include/FMI/fmi_xml_context.h

    include/FMI1/fmi1_xml_model_description.h
    include/FMI1/fmi1_xml_type.h
    include/FMI1/fmi1_xml_unit.h
    include/FMI1/fmi1_xml_vendor_annotations.h
    include/FMI1/fmi1_xml_variable.h
    include/FMI1/fmi1_xml_capabilities.h

    include/FMI2/fmi2_xml_model_description.h
    include/FMI2/fmi2_xml_model_structure.h
    include/FMI2/fmi2_xml_type.h
    include/FMI2/fmi2_xml_unit.h
    include/FMI2/fmi2_xml_variable.h
 )
