set(MINIZIP_SOURCE
  ${MINIZIP_SRC_DIR}/ioapi.c
  ${MINIZIP_SRC_DIR}/miniunz.c
  ${MINIZIP_SRC_DIR}/minizip.c
  #${MINIZIP_SRC_DIR}/mztools.c
  ${MINIZIP_SRC_DIR}/unzip.c
  ${MINIZIP_SRC_DIR}/zip.c
)

if(WIN32)
    set(MINIZIP_SOURCE ${MINIZIP_SOURCE} ${MINIZIP_SRC_DIR}/iowin32.c)
endif(WIN32)

set(MINIZIP_HEADERS
  ${MINIZIP_SRC_DIR}/crypt.h
  ${MINIZIP_SRC_DIR}/ioapi.h  
  ${MINIZIP_SRC_DIR}/miniunz.h
  #${MINIZIP_SRC_DIR}/mztools.h
  ${MINIZIP_SRC_DIR}/unzip.h
)

if(WIN32)
    set(MINIZIP_HEADERS ${MINIZIP_HEADERS} ${MINIZIP_SRC_DIR}/iowin32.h)
endif(WIN32)
